package main

import (
	"html/template"
	"os"
)

type User struct {
	Name  string
	Dog   string
	Age   int
	Life  float64
	Slice []string
	Map   map[string]string
	Slave Slave
}

type Slave struct {
	Name  string
	Chain []string
}

func main() {
	t, err := template.ParseFiles("exp/hello.gohtml")
	if err != nil {
		panic(err)
	}

	slave := Slave{
		Name:  "kILL",
		Chain: []string{"sl", "av", "es"},
	}

	data := User{
		Name:  "John Smith",
		Dog:   "Wanko",
		Age:   999,
		Life:  2.345,
		Slice: []string{"a", "b", "c"},
		Map: map[string]string{
			"key1": "val1",
			"key2": "val2",
		},
		Slave: slave,
	}

	err = t.Execute(os.Stdout, data)
	if err != nil {
		panic(err)
	}
}
